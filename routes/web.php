<?php

use Illuminate\Support\Facades\Route;

Route::get('/', function () {
    return view('welcome');
});

Route::middleware([
    'auth:sanctum',
    config('jetstream.auth_session'),
    'verified',
])->group(function () {
    Route::get('/dashboard', function () {
        return view('dashboard');
    })->name('dashboard');
});

Route::get('/admin', function () {
    return view('admin.admin');
})->middleware(['auth:sanctum', 'verified']);


//Route::get('/admin/pulse', function () {
//    return view('admin.pulse');
//})->middleware(['auth:sanctum', 'verified']);
