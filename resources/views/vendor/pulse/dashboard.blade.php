<x-app-layout>
    <x-slot name="header">
        <h2 class="font-semibold text-xl text-gray-800 leading-tight">
            {{ __('Pulse') }}
        </h2>
    </x-slot>

    <div class="py-12">
        <div class="max-w-7xl mx-auto sm:px-6 lg:px-8">
            <div class="bg-white overflow-hidden shadow-xl sm:rounded-lg">
                <x-pulse>
                    <livewire:pulse.servers cols="full"/>

                    <livewire:pulse.usage cols="4" rows="2"/>

                    <livewire:pulse.queues cols="4"/>

                    <livewire:pulse.cache cols="4"/>

                    <livewire:pulse.slow-queries cols="8"/>

                    <livewire:pulse.exceptions cols="6"/>

                    <livewire:pulse.slow-requests cols="6"/>

                    <livewire:pulse.slow-jobs cols="6"/>

                    <livewire:pulse.slow-outgoing-requests cols="6"/>
                </x-pulse>
            </div>
        </div>
    </div>
</x-app-layout>
